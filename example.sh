#!/bin/bash

# The first step is to create the read count histogram for Fixseq
# input.  These flags exclude unmapped reads, secondary alignments,
# and QC failures.  You may also consider excluding high-copy
# sequences like mitochondrial reads.

samtools view -F 772 input.bam | cut -f 3,4 | uniq -c | awk '{print $1}' | sort | uniq -c | sort -n | awk '{print $1","$2}' > input.csv
 
# Add the number of bases with no reads. 
# To use a 'mappable fraction of base' estimate change the print int(x*1 - y)
# to print(x*fraction - y)

samtools view -H input.bam | grep -oP "LN:\d+" | awk -v y=`awk -F , '{x+=$2} END {print x}' input.csv` -F : '{x+=$2} END {print int(x*1 - y)",0"}' >> input.csv

# Next, run methods.r and go through the resulting output.csv file.
# Choose which rounding scheme you'd like to employ, and create a
# remapping count file.

cut -f 2,6 -d "," output.csv | tr "," "\t" | tail -n +2 | sort -n > counts_map.txt

# Now use BEDTools and this small Python script to remap read counts
# by expanding/contracting a bed file.  IMPORTANT: The original bam
# must be coordinate-sorted, since the remapper expects duplicate
# reads to be adjacent to each other in the bam (then bed).

BEDTools-2.16.2/bin/bamToBed -i input.bam | python bed_count_remapper.py counts_map.txt > output.bed

# Finally, run the peak caller or other processing algorithm of your
# choice on output.bed.  You could also pipe the above string directly
# into the peak caller in place of its bed input.