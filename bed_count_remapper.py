#!/usr/bin/env python

# a script to remap read counts received from a stream of bed reads.
# according to the count map you give it, it will add or eliminate
# reads according to the function.
#
# the reads must be in coordinate-sorted order!
#
# usage: cat reads.bed | python bed_count_remapper.py remapping.countmap > reads_remapped.bed
#
# remapping.countmap is a tab/space delimited file where each line is "original count" then "new count"
import sys

remapped = dict(zip(xrange(50), xrange(50)))

if len(sys.argv) > 1:
    remapped = {}
    fin = open(sys.argv[1])
    for line in fin:
        line = map(float, line.strip().split())
        
        remapped[line[0]] = max(1, int(line[1])) # rounding mode
        # remapped[line[0]] = max(1, int(10.0*line[1])) # scaling mode
else:
    print >>sys.stderr, "usage: cat input.bed | python %s counts_map.txt > remapped.bed" % sys.argv[0]
    sys.exit(1)

remapped[0] = 0

maxCount = max(remapped.values())

count = 0
last = ("can't match to this")

for line in sys.stdin:
    chromo, start, stop, name, qual, strand = line.strip().split()
    if (chromo, start, stop, strand) == last:
        count += 1
    else:
        for i in xrange(remapped[count] if remapped.has_key(count) else maxCount):
            print "\t".join((last[0], last[1], last[2], str(i+1), "1000", last[3]))
            
        last = (chromo, start, stop, strand)
        count = 1

if count > 0:
    for i in xrange(remapped[count] if remapped.has_key(count) else maxCount):
        print "\t".join((last[0], last[1], last[2], str(i+1), "1000", last[3]))
