#!/usr/bin/Rscript

source('../functions.r')



old.repos <- getOption("repos")
on.exit(options(repos = old.repos))
new.repos <- old.repos
new.repos["CRAN"] <- "http://cran.stat.ucla.edu"
options(repos = new.repos)

ris <- function(x){if(!require(x,character.only=T)){install.packages(x)}}

#Required packages
ris("statmod")
ris('cobs')
ris('logcondens')

gknots = gauss.quad(40,kind="hermite")

set.seed(1)
####
# test poisson
ptest=c(0.01,0.1,1,10)
pois.test=lapply(ptest,function(mu){
  print(mu)
  countin=rpois(80000,mu)
  lcinit=table(as.vector(countin))
  lcset=as.double(names(lcinit))
  #
  if(length(lcset)>2){
    flcd = fitPLCD(lcset,lcinit,maxit=100,lambda=1e-5)
  #
    trunc.fitted = fittrunc(lcinit,lcset,flcd)
    trp = truncate(lcset,trunc.fitted)
                                        #
    discrete.fitted = fitdiscrete(lcinit,lcset,flcd)
                                        #
    continuous.fitted=fitcont.ll(lcinit,lcset,flcd)
    cont.maps = getContMap(continuous.fitted)
    list(trp,discrete.fitted,cont.maps,flcd,lcinit,lcset)
  }else{
    list(c(0,1),list(0,c(1,2)),list(c(0,1),c(0,1),c(0,1)),NULL,lcinit,lcset)
  }
})

pdf('poisson.map.ut.pdf',10,10)
par(mfrow=c(length(ptest)/2,2))
for(i in 1:length(ptest)){
  plot(pois.test[[i]][[6]],pois.test[[i]][[1]],xlab='original',ylab='mapped',main=paste0('mean = ',ptest[i]))
  points(pois.test[[i]][[6]],pois.test[[i]][[2]][[2]]-1,type='o',col='red',pch=2)
  points(pois.test[[i]][[6]],pois.test[[i]][[3]][[1]],type='o',col='yellow',pch=3)
  points(pois.test[[i]][[6]],pois.test[[i]][[3]][[2]],type='o',col='green',pch=4)
  points(pois.test[[i]][[6]],pois.test[[i]][[3]][[3]],type='o',col='blue',pch=5)
  legend('topleft',pch=c(21,2:5),lwd=1,col=c('black','red','yellow','green','blue'),legend=c('trunc','integer','weighted','weighted.median','weighted.logloss'))
}
dev.off()

epois<-function(x,freq){
  dfun(log(x),sum(x*freq)/sum(freq))
}

pdf('poisson.density.ut.pdf',10,10)
par(mfrow=c(2,2))
for(i in 1:length(ptest)){
  realdens = as.double(log(pois.test[[i]][[5]]/sum(pois.test[[i]][[5]])))
  plot(realdens,epois(pois.test[[i]][[1]],pois.test[[i]][[5]]),xlab='true density',ylab='mapped density',type='o',main=paste0('mean = ',ptest[i]))
  points(realdens,epois(pois.test[[i]][[2]][[2]]-1,pois.test[[i]][[5]]),type='o',col='red',pch=2)
  points(realdens,epois(pois.test[[i]][[3]][[1]],pois.test[[i]][[5]]),type='o',col='yellow',pch=3)
  points(realdens,epois(pois.test[[i]][[3]][[2]],pois.test[[i]][[5]]),type='o',col='green',pch=4)
  points(realdens,epois(pois.test[[i]][[3]][[3]],pois.test[[i]][[5]]),type='o',col='blue',pch=5)
  legend('topleft',pch=c(21,2:5),lwd=1,col=c('black','red','yellow','green','blue'),legend=c('trunc','integer','weighted','weighted.median','weighted.logloss'))
}
dev.off()

mu=0.1
sztest = c(0.01,0.1,1,10)
set.seed(1)
nbin.test=lapply(sztest,function(size){
  countin=rnbinom(80000,mu=mu,size=size)
  lcinit=table(as.vector(countin))
  lcset=as.double(names(lcinit))
  #
  if(length(lcset)>2){
    flcd = fitPLCD(lcset,lcinit,maxit=100,lambda=1e-5)
  #
    trunc.fitted = fittrunc(lcinit,lcset,flcd)
    trp = truncate(lcset,trunc.fitted)
                                        #
    discrete.fitted = fitdiscrete(lcinit,lcset,flcd)
                                        #
    continuous.fitted=fitcont.ll(lcinit,lcset,flcd)
    cont.maps = getContMap(continuous.fitted)
    list(trp,discrete.fitted,cont.maps,flcd,lcinit,lcset)
  }else{
    list(c(0,1),list(0,c(1,2)),list(c(0,1),c(0,1),c(0,1)),NULL,lcinit,lcset)
  }
})

pdf('nbin.map.ut.pdf',10,10)
par(mfrow=c(length(ptest)/2,2))
for(i in 1:length(ptest)){
  plot(nbin.test[[i]][[6]],nbin.test[[i]][[1]],xlab='original',ylab='mapped',type='o',ylim=range(c(nbin.test[[i]][[1]],nbin.test[[i]][[2]][[2]],do.call(c,nbin.test[[i]][[3]]))),main=paste0('dispersion=',mu^2/ptest[i]))
  points(nbin.test[[i]][[6]],nbin.test[[i]][[2]][[2]]-1,type='o',col='red',pch=2)
  points(nbin.test[[i]][[6]],nbin.test[[i]][[3]][[1]],type='o',col='yellow',pch=3)
  points(nbin.test[[i]][[6]],nbin.test[[i]][[3]][[2]],type='o',col='green',pch=4)
  points(nbin.test[[i]][[6]],nbin.test[[i]][[3]][[3]],type='o',col='blue',pch=5)
  legend('topleft',pch=c(21,2:5),lwd=1,col=c('black','red','yellow','green','blue'),legend=c('trunc','integer','weighted','weighted.median','weighted.logloss'))
}
dev.off()

pdf('nbin.density.ut.pdf',10,10)
par(mfrow=c(2,2))
for(i in 1:length(ptest)){
  realdens = as.double(log(nbin.test[[i]][[5]]/sum(nbin.test[[i]][[5]])))
  plot(realdens,epois(nbin.test[[i]][[1]],nbin.test[[i]][[5]]),xlab='true density',ylab='mapped density',type='o',main=paste0('dispersion=',mu^2/ptest[i]),ylim=c(-15,0))
  points(realdens,epois(nbin.test[[i]][[2]][[2]]-1,nbin.test[[i]][[5]]),type='o',col='red',pch=2)
  points(realdens,epois(nbin.test[[i]][[3]][[1]],nbin.test[[i]][[5]]),type='o',col='yellow',pch=3)
  points(realdens,epois(nbin.test[[i]][[3]][[2]],nbin.test[[i]][[5]]),type='o',col='green',pch=4)
  points(realdens,epois(nbin.test[[i]][[3]][[3]],nbin.test[[i]][[5]]),type='o',col='blue',pch=5)
  legend('topleft',pch=c(21,2:5),lwd=1,col=c('black','red','yellow','green','blue'),legend=c('trunc','integer','weighted','weighted.median','weighted.logloss'))
}
dev.off()
